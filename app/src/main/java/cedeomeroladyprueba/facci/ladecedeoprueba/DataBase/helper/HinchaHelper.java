package cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.item.HinchaItem;

public class HinchaHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Hincha.db";

    public HinchaHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(HinchaItem.HinchaElementEntry.CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(HinchaItem.HinchaElementEntry.DELETE_TABLE);
        onCreate(db);
    }
}
