package cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.item;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import java.util.ArrayList;

import cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.Modelo.Hincha;
import cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.helper.HinchaHelper;

public class HinchaItem {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private HinchaHelper dbHelper;

    public HinchaItem(Context context) {
        dbHelper = new HinchaHelper(context);

    }

    public static abstract class HinchaElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "Profesores";
        public static final String COLUMN_NAME_CEDULA = "cedula";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_APELLIDO = "apellido";
        public static final String COLUMN_NAME_FECHA_NACIMIENTO = "fecha_nacimiento";
        public static final String COLUMN_NAME_EQUIPO = "equipo";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_CEDULA + TEXT_TYPE +  COMMA_SEP + COLUMN_NAME_NOMBRE  + TEXT_TYPE+ COMMA_SEP  + COLUMN_NAME_APELLIDO + TEXT_TYPE
                + COMMA_SEP + COLUMN_NAME_FECHA_NACIMIENTO + TEXT_TYPE + COMMA_SEP + COLUMN_NAME_EQUIPO + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public void inserProf(String cedula, String nombre, String apellido, String Fecha, String equipo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HinchaElementEntry.COLUMN_NAME_CEDULA, cedula);
        contentValues.put(HinchaElementEntry.COLUMN_NAME_NOMBRE, nombre);
        contentValues.put(HinchaElementEntry.COLUMN_NAME_APELLIDO, apellido);
        contentValues.put(HinchaElementEntry.COLUMN_NAME_FECHA_NACIMIENTO, Fecha);
        contentValues.put(HinchaElementEntry.COLUMN_NAME_EQUIPO, equipo);
        dbHelper.getWritableDatabase().insert(HinchaElementEntry.TABLE_NAME, null, contentValues);


    }

    public ArrayList<Hincha> getAllItems() {

        ArrayList<Hincha> hinchaItems = new ArrayList<>();

        String[] allColumns = { HinchaElementEntry._ID,
                HinchaElementEntry.COLUMN_NAME_CEDULA, HinchaElementEntry.COLUMN_NAME_NOMBRE, HinchaElementEntry.COLUMN_NAME_APELLIDO
                , HinchaElementEntry.COLUMN_NAME_FECHA_NACIMIENTO, HinchaElementEntry.COLUMN_NAME_EQUIPO};

        Cursor cursor = dbHelper.getReadableDatabase().query(
                HinchaElementEntry.TABLE_NAME,    // The table to query
                allColumns,                         // The columns to return
                null,                               // The columns for the WHERE clause
                null,                               // The values for the WHERE clause
                null,                               // don't group the rows
                null,                               // don't filter by row groups
                null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            Hincha hinchaIte = new Hincha(getItemId(cursor), getCedula(cursor), getNombre(cursor), getApellido(cursor)
                    , getFecha(cursor), getEquipo(cursor));
            hinchaItems.add(hinchaIte);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return hinchaItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(HinchaElementEntry._ID));
    }

    private String getCedula(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(HinchaElementEntry.COLUMN_NAME_CEDULA));
    }

    private String getNombre(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(HinchaElementEntry.COLUMN_NAME_NOMBRE));
    }

    private String getApellido(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(HinchaElementEntry.COLUMN_NAME_APELLIDO));
    }

    private String getFecha(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(HinchaElementEntry.COLUMN_NAME_FECHA_NACIMIENTO));
    }

    private String getEquipo(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(HinchaElementEntry.COLUMN_NAME_EQUIPO));
    }
}

