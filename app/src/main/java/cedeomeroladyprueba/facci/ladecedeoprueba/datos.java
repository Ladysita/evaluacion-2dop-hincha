package cedeomeroladyprueba.facci.ladecedeoprueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.item.HinchaItem;

public class datos extends AppCompatActivity implements View.OnClickListener {

    private EditText cedula, nombres, apellidos, fechaNac, equipo;
    private Button agregacion, consulta;
    private HinchaItem hinchaItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);
        cedula = findViewById(R.id.Cedula);
        nombres = findViewById(R.id.nombre);
        apellidos = findViewById(R.id.apellidos);
        fechaNac = findViewById(R.id.fechaNac);
        equipo= findViewById(R.id.equipo);

        agregacion = findViewById(R.id.agregar);
        consulta = findViewById(R.id.consultar);
        consulta.setOnClickListener(this);
        agregacion.setOnClickListener(this);
        hinchaItem = new HinchaItem(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.agregar:
                if (cedula.getText().toString().isEmpty()){
                    cedula.setError(getString(R.string.mensaje));
                }if (nombres.getText().toString().isEmpty()){
                nombres.setError(getString(R.string.mensaje));
            }if (apellidos.getText().toString().isEmpty()){
                apellidos.setError(getString(R.string.mensaje));
            }if (fechaNac.getText().toString().isEmpty()) {
                fechaNac.setError(getString(R.string.mensaje));
            }if (equipo.getText().toString().isEmpty()){
                equipo.setError(getString(R.string.mensaje));
            }else {
                hinchaItem.inserProf(cedula.getText().toString(), nombres.getText().toString(), apellidos.getText().toString(), fechaNac.getText().toString(), equipo.getText().toString());
                startActivity(new Intent(datos.this, MainActivity.class));
                Toast.makeText(this, "PROFESOR INSERTADO", Toast.LENGTH_SHORT).show();
            }
                break;

            case R.id.consultar:
                startActivity(new Intent(datos.this, MainActivity.class));
                break;
        }
    }
}