package cedeomeroladyprueba.facci.ladecedeoprueba.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.Modelo.Hincha;
import cedeomeroladyprueba.facci.ladecedeoprueba.R;


public class AdapterHincha extends ArrayAdapter<Hincha> {

        private Context context;
        private ArrayList<Hincha> hinchaArray;

        public AdapterHincha(Context context, ArrayList<Hincha> hinchaArray) {
            super(context, R.layout.list_proff);
            this.context = context;
            this.hinchaArray = hinchaArray;
        }

        @Override
        public int getCount() {
            return hinchaArray.size();
        }

        @Override
        public Hincha getItem(int position) {
            return  hinchaArray.get(position);
        }
        @Override
        public long getItemId(int position) {
            return hinchaArray.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            final ViewHolder viewHolder;

            if (convertView == null || convertView.getTag() == null) {
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(context).inflate(R.layout.list_proff, parent, false);
                viewHolder.mItemName = view.findViewById(R.id.LBLNombre);
                viewHolder.cedula = view.findViewById(R.id.LBLCedula);
                viewHolder.apellido = view.findViewById(R.id.LBLApellido);
                viewHolder.equipo = view.findViewById(R.id.LBLEquipo);
                viewHolder.fecha = view.findViewById(R.id.FechaN);
                view.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                view = convertView;
            }

            // Set text with the item name
            viewHolder.mItemName.setText(hinchaArray.get(position).getNombre());
            viewHolder.cedula.setText(hinchaArray.get(position).getCedula());
            viewHolder.apellido.setText(hinchaArray.get(position).getApellidos());
            viewHolder.fecha.setText(hinchaArray.get(position).getFecha());
            viewHolder.equipo.setText(hinchaArray.get(position).getEquipo());

            return view;
        }

        static class ViewHolder {
            TextView mItemName, cedula, apellido, fecha, equipo;
        }
    }


