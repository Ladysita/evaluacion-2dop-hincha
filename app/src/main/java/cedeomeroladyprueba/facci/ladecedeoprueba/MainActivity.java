package cedeomeroladyprueba.facci.ladecedeoprueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import cedeomeroladyprueba.facci.ladecedeoprueba.Adapter.AdapterHincha;
import cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.Modelo.Hincha;
import cedeomeroladyprueba.facci.ladecedeoprueba.DataBase.item.HinchaItem;

public class MainActivity extends AppCompatActivity {

    private ListView lista;
    private Button guardar;
    private HinchaItem IHincha;
    private AdapterHincha AHincha;
    private ArrayList<Hincha> hinchaArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = (ListView) findViewById(R.id.ListViewH);

        guardar = (Button)findViewById(R.id.AggHincha);
        IHincha = new HinchaItem(this);

        hinchaArrayList.addAll(IHincha.getAllItems());

        AHincha = new AdapterHincha(this, hinchaArrayList);

        lista.setAdapter(AHincha);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, datos.class));
            }
        });
    }
}
